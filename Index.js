const express = require('express')
const path = require('path')
const cors = require('cors')

const userRoute = require('./src/routes/UserRoute')
const storeRoute = require('./src/routes/StoreRoute')
const userLoginRoute = require('./src/routes/UserLoginRoute')
const burgerLogin = require('./src/routes/BurgerLogin')
const dotenv  = require('dotenv').config()

const app = express()
const port = process.env.PORT

app.use(express.json())
app.use(cors())

app.use("/api/static",express.static(path.join(__dirname, 'public')))

app.get('/', (req, res) => {
  res.send('Hallo dunia')
})

app.use('/api/user', userRoute)
app.use('/api/store', storeRoute)
app.use('/api/userLogin', userLoginRoute)
app.use('/api/burgerLogin', burgerLogin)

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})
