const dotenv  = require('dotenv').config()
var jwt = require('jsonwebtoken');

const authMiddleware = (req, res, next)=>{
    let token = req.headers.authorization
    if(token == undefined){
        res.status(403).send();
    }else{
        token = token.split(' ')[1]
        console.log(token)
        try {
            jwt.verify(
                token, 
                process.env.SecretKey,
                (err, decoded)=>{
                    if (err){
                        res.status(403).send();
                    }else{
                        req.body.user_id = decoded.user_id
                        next()
                    }
                });
        } catch(err) {
          res.status(500).send();
        }
    }
}

module.exports = {authMiddleware}