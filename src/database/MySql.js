var mysql = require('mysql')

var connection = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: 'codingid',
  database: 'olevel1'
})

connection.connect((err)=>{
    if(err) console.log(err)

    console.log('Database Connected')
})

module.exports = connection