const express = require("express")
const multer = require("multer")
const CryptoJS = require("crypto-js");
const bcrypt = require('bcrypt');
const dotenv  = require('dotenv').config()
var jwt = require('jsonwebtoken');

const mySql = require('../database/MySql')
const {authMiddleware} = require('../middleware/AuthMiddleware')

const burgerLoginRoute = express.Router()
const secretKey = process.env.SecretKey

const storage = multer.diskStorage({
    destination(req, file, callback) {
      callback(null, './public/image');
    },
    filename(req, file, callback) {
      callback(null, `${file.fieldname}_${Date.now()}_${file.originalname}`);
    },
});
  
const upload = multer({ storage, limits:{fieldSize: 2 * 1024 * 1024 }});

burgerLoginRoute.post('/register', upload.array('photo'), (req, res)=>{
    // console.log(req.headers);
    console.log('file', req.files);
    console.log('body', req.body);
    const {username, password} = req.body;

    let bytes  = CryptoJS.AES.decrypt(password, secretKey);
    let originalText = bytes.toString(CryptoJS.enc.Utf8);
    
    const hash =  bcrypt.hashSync(originalText, 10);
    try{
        mySql.query(
            `insert into ms_userlogin(username, password_user) 
            VALUES ('${username}','${hash}')`,
            (error, results, fields)=>{
                if(error) throw error;

                console.log(results.insertId)   

                //GENERATE QUERY
                let query = `INSERT INTO master_userphoto VALUES `
                req.files.forEach((item,index)=>{
                    query  += `(DEFAULT, '${item.filename}', ${results.insertId})`
                    if(index != req.files.length-1){
                        query+=','
                    }
                })

                console.log(query)
                
                mySql.query(
                    query,
                    (error, results, fields)=>{
                        if(error) throw error;

                        res.status(200).json({
                            message: 'success!',
                        });
                    }
                )
                
            })
    }catch{
        res.status(500).send();
    }
})

burgerLoginRoute.get('/photo',authMiddleware, (req, res)=>{
    const id = req.body.user_id
    try{
        mySql.query(
            `SELECT * FROM master_userphoto WHERE user_id = ${id}`,
            (error, results, fields)=>{
                if(error) throw error;

                res.status(200).json({
                    message: 'success!',
                    data:results
                });
            })
    }catch{
        res.status(500).send();
    }
})


burgerLoginRoute.post('/login', (req, res)=>{
    const {username, password, status} = req.body
    console.log(status)
    let bytes  = CryptoJS.AES.decrypt(password, secretKey);
    let originalText = bytes.toString(CryptoJS.enc.Utf8);

    try{
        mySql.query(
            `SELECT user_id, password_user FROM ms_userlogin WHERE username = '${username}' `,
            (error, results, fields)=>{
                if(error) throw error;

                console.log(results)
                let password = results[0].password_user
                const condition = bcrypt.compareSync(originalText, password);

                if(condition==true){   
                    var token = jwt.sign({ user_id: results[0].user_id }, secretKey);
                    res.status(200).json({
                        message: 'Login!',
                        data:token
                    });
                }else{
                    res.status('401').json({
                        message: 'Invalid Credential'
                    });
                }
            })
    }catch{
        res.status(500).send();
    }
})

burgerLoginRoute.get('/city',authMiddleware, (req, res)=>{
    const id = req.body.user_id
    try{
        mySql.query(
            `SELECT * FROM ms_city`,
            (error, results, fields)=>{
                if(error) throw error;
                console.log(results)
                res.status(200).json({
                    message: 'success!',
                    data:results
                });
            })
    }catch{
        res.status(500).send();
    }
})


burgerLoginRoute.get('/searchCity/:search?',authMiddleware, (req, res)=>{
    const id = req.body.user_id
    const {search} = req.params
    try{
        let query="";
        console.log((search))
        if(search){
            query=`SELECT * FROM ms_city where city_name like '%${search}%'`
        }else{
            query=`SELECT * FROM ms_city`
        }

        mySql.query(
            query,
            (error, results, fields)=>{
                if(error) throw error;
                console.log(results)
                res.status(200).json({
                    message: 'success!',
                    data:results
                });
            })
    }catch{
        res.status(500).send();
    }
})
module.exports = burgerLoginRoute