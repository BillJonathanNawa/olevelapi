const express = require("express")
const userRoute = express.Router()

//READ
userRoute.get('/:id?', (req, res) => {
    const {id} = req.query
    // const {id} = req.params
  
    let hasil = {}
    for(i=0; i<DataUser.length; i++){
      if(DataUser[i].id == id){
        hasil = DataUser[i]
      }
    }
    
    if(id == undefined){
      res.send(DataUser)
    }else if(hasil.id == undefined){
      res.status(200).send("Data Not Found")
    }else{
      res.send(hasil)
    }
})

//CREATE
userRoute.post('/Create', function (req, res) {
    const {name, job} = req.body
    const {harusada} = req.headers
  
    if(harusada == "PIKED.com"){
      if(!name || !job){
        res.status(400).send()
      }else{
        const lastID = DataUser[DataUser.length-1].id
        const data = {
          id:lastID+1,
          username:name,
          job:job
        }
        DataUser.push(data)
        res.status(201).send({
          message:"Data Created",
          data:data
        })
      }
    }else{
      res.status(400).send("Bad Request");
    }
})
  
//DELETE
userRoute.post("/Delete/:id", (req,res) => {
    const {id}= req.params;
  
    const index = DataUser.findIndex(item => item.id == id)
  
    if(index==-1){  
      res.status(404).send("Data Not Found")
    }else{
      DataUser.splice(index,1);
      res.send({
        message:"Data Deleted",
        data: DataUser
      })
    }
})
  
//UPDATE
userRoute.put('/Update/:id',(req,res)=>{
    const {name, job} = req.body;
    const {id} = req.params;
  
    if(id && name && job){
      const index = DataUser.findIndex(item => item.id == id)
  
      if(index == -1){
        res.status(404).send("Data Not Found")
      }else{
        DataUser[index].username = name;
        DataUser[index].job = job;
  
        res.send({
          message:"Data Updated",
          data: DataUser
        })
      }
    }else{
      res.status(400).send("Bad Request")
    }
})

module.exports = userRoute

const DataUser = [
    {
      id:1,
      username:"Bill",
      job:"Developer"
    },
    {
      id:2,
      username:"Fai",
      job:"Developer"
    },
    {
      id:3,
      username:"Demas",
      job:"Developer"
    },
    {
      id:4,
      username:"Mego",
      job:"Developer"
    },
    {
      id:5,
      username:"Ropi",
      job:"Developer"
    },
  ]